function change(){
	const moneda1 = document.getElementById('div1');
	const moneda2 = document.getElementById('div2');
	let contenido = "";
	
    if(parseInt(moneda1.value) == 1){
        contenido = `<option value="6">Dólar estadounidense</option>
			<option value="7">Dolar canadiense</option>
			<option value="8">Euro</option>`
    }if(parseInt(moneda1.value) == 2){
        contenido = `<option value="5">Peso mexicano</option>
			<option value="7">Dolar canadiense</option>
			<option value="8">Euro</option>`
    }if(parseInt(moneda1.value) == 3){
        contenido = `<option value="5">Peso mexicano</option>
			<option value="6">Dolar estadounidense</option>
			<option value="8">Euro</option>`
    }if(parseInt(moneda1.value) == 4){
        contenido = `<option value="5">Peso mexicano</option>
			<option value="6">Dolar estadounidense</option>
			<option value="7">Dolar canadiense</option>`
    }

	moneda2.innerHTML = contenido;
}


function calcular(){
    let valor = document.getElementById('cantidad').value
    let moneda1 = document.getElementById('div1')
    let moneda2 = document.getElementById('div2')
    let subtotal = document.getElementById('subtotal')
    let comision = document.getElementById('comision')
    let pagar = document.getElementById('pagar')

    // Pesos
    if(parseInt(moneda1.value) == 1 && parseInt(moneda2.value) == 6){
        subtotal.value = valor / 19.85
    }else if(parseInt(moneda1.value) == 1 && parseInt(moneda2.value) == 7){
        subtotal.value = valor / 14.70
    }else if(parseInt(moneda1.value) == 1 && parseInt(moneda2.value) == 8){
        subtotal.value = valor / 20.05
    }

    // Dolares
    else if(parseInt(moneda1.value) == 2 && parseInt(moneda2.value) == 5){
        subtotal.value = valor * 19.85
    }else if(parseInt(moneda1.value) == 2 && parseInt(moneda2.value) == 7){
        subtotal.value = valor * 1.35
    }else if(parseInt(moneda1.value) == 2 && parseInt(moneda2.value) == 8){
        subtotal.value = valor * 0.99
    }

    // Dolares canadiences
    else if(parseInt(moneda1.value) == 3 && parseInt(moneda2.value) == 5){
        subtotal.value =  (valor / 1.35) * 19.85
    }else if(parseInt(moneda1.value) == 3 && parseInt(moneda2.value) == 6){
        subtotal.value = (valor / 1.35)
    }else if(parseInt(moneda1.value) == 3 && parseInt(moneda2.value) == 8){
        subtotal.value =  (valor / 1.35) * 0.99
    }

    // Euro
    else if(parseInt(moneda1.value) == 4 && parseInt(moneda2.value) == 5){
        subtotal.value = (valor / 0.99) * 19.85
    }else if(parseInt(moneda1.value) == 4 && parseInt(moneda2.value) == 6){
        subtotal.value = (valor / 0.99)
    }else if(parseInt(moneda1.value) == 4 && parseInt(moneda2.value) == 7){
        subtotal.value = (valor / 0.99) * 1.35
    }
    
    comision.value = (subtotal.value * .03).toFixed(2)
    pagar.value = (parseFloat(subtotal.value) + parseFloat(comision.value)).toFixed(2)
    
    console.log(valor)
    console.log(subtotal.value)
    console.log(comision.value)
    console.log(pagar.value)


}

let subtotalF = 0
let comisionF = 0
let pagarF = 0

function registrar(){
    let valor = document.getElementById('cantidad')
    let moneda1 = document.getElementById('div1')
    let moneda2 = document.getElementById('div2')
    let subtotal = document.getElementById('subtotal')
    let comision = document.getElementById('comision')
    let pagar = document.getElementById('pagar')
    let registros = document.getElementById('registros')
    let totales = document.getElementById('totales')
    

    registros.innerText += `Fueron ${valor.value} de la divisa ${moneda1.value} a ${moneda2.value} con un total ${subtotal.value} con una comision ${comision.value} y el total a pagar es ${pagar.value} \n`;

    subtotalF += parseFloat(subtotal.value)
    comisionF += parseFloat(comision.value)
    pagarF += parseFloat(pagar.value)


    totales.innerText = `Sub total: ${parseFloat(subtotalF)} Comision: ${parseFloat(comisionF)} Total: ${parseFloat(pagarF)}` 

}

function borrar(){
    let registros = document.getElementById('registros')
    let totales = document.getElementById('totales')
	let subtotal = document.getElementById('subtotal').value = ''
    let comision = document.getElementById('comision').value = ''
    let pagar = document.getElementById('pagar').value = ''
    let cantidad = document.getElementById('cantidad').value = ''

    registros.innerHTML = ''
    totales.innerHTML = ''
    subtotal.innerHTML = ''
}